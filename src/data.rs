// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub enum SchoolCode {
    ALE,
    DSE,
    OMS,
    OHS,
    WSE,
    #[serde(other)]
    ERR,
}

impl Default for SchoolCode {
    fn default() -> Self {
        SchoolCode::ERR
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Record {
    pub firstname: String,
    pub lastname: String,
    pub uid: u64,
    pub grade: String,
    #[serde(default)]
    pub school_short_code: SchoolCode,
    pub active: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChangedPasswordRecord {
    pub firstname: String,
    pub lastname: String,
    pub grade: String,
    pub school_short_code: SchoolCode,
    pub password: String,
}
