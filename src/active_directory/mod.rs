// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub mod computer;
pub mod object;
pub mod organizational_unit;
pub mod server;
pub mod user;

use std::sync::RwLock;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref TEST_RUN: RwLock<bool> = RwLock::new(true);
}
