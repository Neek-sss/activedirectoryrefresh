// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use ldap3::LdapConn;

pub struct OrganizationalUnit<'a> {
    pub connection: &'a mut LdapConn,
    pub dn: String,
}
