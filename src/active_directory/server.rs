// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::active_directory::object::Object;
use ldap3::{LdapConn, LdapConnSettings, Scope, SearchEntry};
use log::info;
use maplit::hashset;
use std::error::Error;

pub enum ActiveDirectoryFlag {
    DisabledAccount,
    NormalAccount,
}

impl ActiveDirectoryFlag {
    pub fn to_u32(&self) -> i32 {
        match self {
            ActiveDirectoryFlag::DisabledAccount => 0x0202,
            ActiveDirectoryFlag::NormalAccount => 0x0200,
        }
    }
}

pub struct Server {
    connection: LdapConn,
}

impl Server {
    pub fn new(server: &str, account: &str, secret: &str) -> Result<Self, Box<dyn Error>> {
        info!("Connecting to {} with user {}", server, account);

        let mut connection = LdapConn::with_settings(
            LdapConnSettings::new()
                .set_starttls(true)
                .set_no_tls_verify(true),
            server,
        )?;
        connection.simple_bind(account, secret)?.success()?;

        info!("Connection successful");

        Ok(Self { connection })
    }

    pub fn add_user(
        &mut self,
        dn: &str,
        first_name: &str,
        last_name: &str,
        graduation_year: u32,
    ) -> Result<(), Box<dyn Error>> {
        info!("Adding user {} {} to {}", first_name, last_name, dn);

        if !*(crate::active_directory::TEST_RUN.read()?) {
            let email = format!("{}{}@otsegops.org", first_name, last_name);
            let name = format!("{} {}", first_name, last_name);
            let username = format!("{}{}", first_name, last_name);
            let drive = format!(
                "\\\\vm-wsvr-fs1\\Students\\{}\\{}{}\\",
                graduation_year, first_name, last_name
            );
            let disable_string = format!("{}", ActiveDirectoryFlag::DisabledAccount.to_u32());

            self.connection
                .add(
                    dn,
                    vec![
                        ("name".as_bytes(), hashset![name.as_str().as_bytes()]),
                        ("pwdLastSet".as_bytes(), hashset!["0".as_bytes()]),
                        (
                            "accountExpires".as_bytes(),
                            hashset!["9223372036854775807".as_bytes()],
                        ),
                        ("givenName".as_bytes(), hashset![first_name.as_bytes()]),
                        ("distinguishedName".as_bytes(), hashset![dn.as_bytes()]),
                        (
                            "sAMAccountName".as_bytes(),
                            hashset![username.as_str().as_bytes()],
                        ),
                        (
                            "objectClass".as_bytes(),
                            hashset![
                            "organizationalPerson".as_bytes(),
                            "person".as_bytes(),
                            "top".as_bytes(),
                            "user".as_bytes()
                        ],
                        ),
                        ("sn".as_bytes(), hashset![last_name.as_bytes()]),
                        (
                            "objectCategory".as_bytes(),
                            hashset![
                            "CN=Person,CN=Schema,CN=Configuration,DC=otsegops,DC=org".as_bytes()
                        ],
                        ),
                        ("homeDrive".as_bytes(), hashset!["H:".as_bytes()]),
                        ("displayName".as_bytes(), hashset![name.as_str().as_bytes()]),
                        ("mail".as_bytes(), hashset![email.as_str().as_bytes()]),
                        (
                            "userAccountControl".as_bytes(),
                            hashset![disable_string.as_bytes()],
                        ),
                        ("cn".as_bytes(), hashset![name.as_str().as_bytes()]),
                        (
                            "homeDirectory".as_bytes(),
                            hashset![drive.as_str().as_bytes()],
                        ),
                    ],
                )?
                .non_error()?;
        }

        Ok(())
    }

    pub fn get_active_directory_object(&mut self, dn: &str) -> Result<Object, Box<dyn Error>> {
        Object::new(&mut self.connection, dn)
    }

    pub fn simple_search(
        &mut self,
        ou: &str,
        filter: &str,
    ) -> Result<Option<SearchEntry>, Box<dyn Error>> {
        // info!("Searching {} for {}", ou, filter);

        let search_result = self
            .connection
            .search(ou, Scope::Subtree, filter, vec!["*", "+"])?;

        Ok(search_result.non_error().ok().and_then(|result_entry| {
            result_entry
                .0
                .get(0)
                .and_then(|first_entry| Some(SearchEntry::construct(first_entry.clone())))
        }))
    }

    pub fn loop_over_objects(
        &mut self,
        ou: &str,
        func: &dyn Fn(SearchEntry) -> Result<bool, Box<dyn Error>>,
    ) -> Result<usize, Box<dyn Error>> {
        info!("Looping over {}", ou);

        let mut ret = 0;
        let mut entry_stream = self.connection.streaming_search(
            ou,
            Scope::Subtree,
            "(&(objectCategory=person)(objectClass=user))",
            vec!["+", "*"],
        )?;
        let mut entry_option = entry_stream.next()?;
        while entry_option.is_some() {
            if let Some(entry) = entry_option {
                let search = SearchEntry::construct(entry);

                if func(search)? {
                    ret += 1;
                }

                entry_option = entry_stream.next()?;
            }
        }

        Ok(ret)
    }
}
