// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::active_directory::{
    computer::Computer, organizational_unit::OrganizationalUnit, user::User,
};
use ldap3::{LdapConn, Mod, Scope, SearchEntry};
use log::{info, warn};
use maplit::hashset;
use std::{
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
};

pub enum Object<'a> {
    User(User<'a>),
    Computer(Computer<'a>),
    OrganizationalUnit(OrganizationalUnit<'a>),
}

#[derive(Debug, Copy, Clone)]
pub enum ActiveDirectoryError {
    ObjectError,
}

impl Display for ActiveDirectoryError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Self::ObjectError => write!(f, "Not a known Active Directory object"),
        }
    }
}

impl Error for ActiveDirectoryError {}

pub fn simple_search(
    connection: &mut LdapConn,
    ou: &str,
    filter: &str,
) -> Result<Option<SearchEntry>, Box<dyn Error>> {
    let search_result = connection.search(ou, Scope::Subtree, filter, vec!["*", "+"])?;

    Ok(search_result.non_error().ok().and_then(|result_entry| {
        result_entry
            .0
            .get(0)
            .and_then(|first_entry| Some(SearchEntry::construct(first_entry.clone())))
    }))
}

impl<'a> Object<'a> {
    pub fn new(connection: &'a mut LdapConn, dn: &str) -> Result<Self, Box<dyn Error>> {
        let user_option = simple_search(connection, dn, "(objectClass=user)")?;
        let computer_option = simple_search(connection, dn, "(objectClass=computer)")?;
        let ou_option = simple_search(connection, dn, "(objectClass=organizationalUnit)")?;

        if computer_option.is_some() {
            Ok(Self::Computer(Computer {
                connection,
                dn: dn.to_string(),
            }))
        } else if ou_option.is_some() {
            Ok(Self::OrganizationalUnit(OrganizationalUnit {
                connection,
                dn: dn.to_string(),
            }))
        } else if user_option.is_some() {
            Ok(Self::User(User {
                connection,
                dn: dn.to_string(),
            }))
        } else {
            Err(Box::new(ActiveDirectoryError::ObjectError))
        }
    }

    pub fn as_user(&mut self) -> &mut User<'a> {
        if let Object::User(user) = self {
            user
        } else {
            panic!("Object is not a user")
        }
    }

    pub fn as_computer(&mut self) -> &mut Computer<'a> {
        if let Object::Computer(computer) = self {
            computer
        } else {
            panic!("Object is not a computer")
        }
    }

    pub fn as_organizational_unit(&mut self) -> &mut OrganizationalUnit<'a> {
        if let Object::OrganizationalUnit(ou) = self {
            ou
        } else {
            panic!("Object is not an organizational unit")
        }
    }

    pub fn add_to_group(&mut self, group_dn: &str) -> Result<(), Box<dyn Error>> {
        match self {
            Object::Computer(Computer { connection, dn })
            | Object::OrganizationalUnit(OrganizationalUnit { connection, dn })
            | Object::User(User { connection, dn }) => {
                let group_option = simple_search(connection, group_dn, "(objectCategory=group)")?;
                let object_option = simple_search(
                    connection,
                    dn,
                    "(&(objectCategory=person)(objectClass=user))",
                )?;

                if group_option.is_none() {
                    warn!("Group {} does not exist", group_dn);
                }

                if object_option.is_none() {
                    warn!("Object {} does not exist", dn);
                }

                if let (Some(group), Some(_)) = (group_option, object_option) {
                    if let Some(users_in_group) = group.attrs.get("member") {
                        if !users_in_group.contains(&dn.to_string()) {
                            info!("Adding {} to {}", dn, group_dn);

                            if !*(crate::active_directory::TEST_RUN.read()?) {
                                connection
                                    .modify(group_dn, vec![Mod::Add("member", hashset![dn.as_str()])])?
                                    .non_error()?;
                            }
                        }
                    }
                }
            }
        }

        Ok(())
    }

    pub fn remove_from_group(&mut self, group_dn: &str) -> Result<(), Box<dyn Error>> {
        match self {
            Object::Computer(Computer { connection, dn })
            | Object::OrganizationalUnit(OrganizationalUnit { connection, dn })
            | Object::User(User { connection, dn }) => {
                let group_option = simple_search(connection, group_dn, "(objectCategory=group)")?;
                let object_option = simple_search(
                    connection,
                    dn,
                    "(&(objectCategory=person)(objectClass=user))",
                )?;

                if group_option.is_none() {
                    warn!("Group {} does not exist", group_dn);
                }

                if object_option.is_none() {
                    warn!("Object {} does not exist", dn);
                }

                if let (Some(group), Some(_)) = (group_option, object_option) {
                    if let Some(users_in_group) = group.attrs.get("member") {
                        if users_in_group.contains(&dn.to_string()) {
                            info!("Removing {} from {}", dn, group_dn);

                            if !*(crate::active_directory::TEST_RUN.read()?) {
                                connection
                                    .modify(
                                        group_dn,
                                        vec![Mod::Delete("member", hashset![dn.as_str()])],
                                    )?
                                    .non_error()?;
                            }
                        }
                    }
                }
            }
        }

        Ok(())
    }

    pub fn move_object(&mut self, ou: &str) -> Result<(), Box<dyn Error>> {
        match self {
            Object::Computer(Computer { connection, dn })
            | Object::OrganizationalUnit(OrganizationalUnit { connection, dn })
            | Object::User(User { connection, dn }) => {
                let name = dn.chars().take_while(|&c| c != ',').collect::<String>();
                let rdn = format!("{},{}", name, ou);

                if dn.clone() != rdn {
                    info!("Moving {} to {}", dn, rdn);

                    if !*(crate::active_directory::TEST_RUN.read()?) {
                        connection
                            .modifydn(dn, &name, true, Some(ou))?
                            .non_error()?;
                    }
                }
            }
        }

        Ok(())
    }
}
