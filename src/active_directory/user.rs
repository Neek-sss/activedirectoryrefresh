// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::active_directory::object::simple_search;
use crate::active_directory::server::ActiveDirectoryFlag;
use ldap3::{LdapConn, Mod};
use log::info;
use maplit::hashset;
use std::error::Error;

fn str_to_encoded_password(password: &str) -> Vec<u8> {
    let parenthed_password = format!("\"{}\"", password);
    let encoded_password = parenthed_password.encode_utf16();

    let mut bytes = Vec::new();
    for word in encoded_password {
        bytes.push(word as u8);
        bytes.push((word >> 8) as u8);
    }
    bytes
}

pub struct User<'a> {
    pub connection: &'a mut LdapConn,
    pub dn: String,
}

impl<'a> User<'a> {
    pub fn change_password(&mut self, password: &str) -> Result<(), Box<dyn Error>> {
        info!("Change password for {} to {}", self.dn, password);

        if !*(crate::active_directory::TEST_RUN.read()?) {
            str_to_encoded_password("new");

            let password = str_to_encoded_password(password);
            self.connection
                .modify(
                    &self.dn,
                    vec![
                        Mod::Replace("pwdLastSet".as_bytes(), hashset!["-1".as_bytes()]),
                        Mod::Replace("unicodePwd".as_bytes(), hashset![password.as_slice()]),
                    ],
                )?
                .non_error()?;
        }

        Ok(())
    }

    pub fn force_change_password_on_next_login(&mut self) -> Result<(), Box<dyn Error>> {
        let object_option = simple_search(
            &mut self.connection,
            &self.dn,
            "(&(objectCategory=person)(objectClass=user))",
        )?;

        if let Some(object) = object_option {
            if object.attrs["pwdLastSet"][0] != "0" {
                info!("Force {} to change password on next login", self.dn);

                if !*(crate::active_directory::TEST_RUN.read()?) {
                    self.connection
                        .modify(&self.dn, vec![Mod::Replace("pwdLastSet", hashset!["0"])])?
                        .non_error()?;
                }
            }
        }

        Ok(())
    }

    pub fn no_change_password_on_next_login(&mut self) -> Result<(), Box<dyn Error>> {
        let object_option = simple_search(
            &mut self.connection,
            &self.dn,
            "(&(objectCategory=person)(objectClass=user))",
        )?;

        if let Some(object) = object_option {
            if object.attrs["pwdLastSet"][0] == "0" {
                info!("No force change password for {}", self.dn);

                if !*(crate::active_directory::TEST_RUN.read()?) {
                    self.connection
                        .modify(&self.dn, vec![Mod::Replace("pwdLastSet", hashset!["-1"])])?
                        .non_error()?;
                }
            }
        }

        Ok(())
    }

    pub fn enable_user(&mut self) -> Result<(), Box<dyn Error>> {
        let enable_string = format!("{}", ActiveDirectoryFlag::NormalAccount.to_u32());

        let object_option = simple_search(
            &mut self.connection,
            &self.dn,
            "(&(objectCategory=person)(objectClass=user))",
        )?;

        if let Some(object) = object_option {
            if object.attrs["userAccountControl"][0] != enable_string {
                info!("Enabling {}", self.dn);

                if !*(crate::active_directory::TEST_RUN.read()?) {
                    self.connection
                        .modify(
                            &self.dn,
                            vec![Mod::Replace(
                                "userAccountControl",
                                hashset![enable_string.as_str()],
                            )],
                        )?
                        .non_error()?;
                }
            }
        }

        Ok(())
    }

    pub fn disable_user(&mut self) -> Result<(), Box<dyn Error>> {
        let disable_string = format!("{}", ActiveDirectoryFlag::DisabledAccount.to_u32());

        let object_option = simple_search(
            &mut self.connection,
            &self.dn,
            "(&(objectCategory=person)(objectClass=user))",
        )?;

        if let Some(object) = object_option {
            if object.attrs["userAccountControl"][0] != disable_string {
                info!("Disabling {}", self.dn);

                if !*(crate::active_directory::TEST_RUN.read()?) {
                    self.connection
                        .modify(
                            &self.dn,
                            vec![Mod::Replace(
                                "userAccountControl",
                                hashset![disable_string.as_str()],
                            )],
                        )?
                        .non_error()?;
                }
            }
        }

        Ok(())
    }
}
