// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use ldap3::SearchEntry;
use std::{
    collections::HashMap,
    error::Error,
    fmt::{Display, Formatter, Result as FmtResult},
    fs,
    io::Write,
    iter::FromIterator,
};
use clap::{App, Arg};

use toml;
#[macro_use]
extern crate serde_derive;
use chrono::{Datelike, Utc};
use csv;
use log::{self, error, info, warn};
#[cfg(debug_assertions)]
use simple_logger;
#[cfg(not(debug_assertions))]
use simple_logging;

mod active_directory;
use active_directory::server::Server;
mod config;
use config::ConfigFile;
mod data;
use data::{ChangedPasswordRecord, Record};

fn read_toml_file(filename: &str) -> Result<ConfigFile, Box<dyn Error>> {
    Ok(toml::from_str::<ConfigFile>(&fs::read_to_string(
        filename,
    )?)?)
}

fn read_csv_file(filename: &str) -> Result<HashMap<String, Record>, Box<dyn Error>> {
    let mut file_data = csv::Reader::from_path(filename)?;
    let mut data = HashMap::new();
    for record_result in file_data.deserialize::<Record>() {
        match record_result {
            Ok(record) => {
                data.insert(record.firstname.clone() + &record.lastname.clone(), record);
            }
            Err(error) => {
                if let Some(pos) = error.position() {
                    warn!("Malformed record in line {}!", pos.line());
                }
            }
        }
    }
    Ok(data)
}

fn graduation_year(config_file: &ConfigFile, grade: &str) -> u32 {
    let current_year = Utc::now().year() as u32;
    (14 - config_file
        .config
        .grade_order
        .iter()
        .position(|g| g == grade)
        .unwrap() as u32)
        + current_year
}

fn get_unknown_users(
    ad: &mut Server,
    config_file: &ConfigFile,
    names: &[String],
) -> Result<(), Box<dyn Error>> {
    let mut total_num_unknown_users = 0;
    for grade in config_file.grades.iter().filter(|grade| {
        grade
            .1
            .disabled
            .filter(|&should_disable| should_disable)
            .is_none()
    }) {
        info!(
            "Searching for unknown users in grade {}",
            graduation_year(config_file, grade.0)
        );
        let num_unknown_users = ad.loop_over_objects(
            format!(
                "{},{},{}",
                grade.1.ou, config_file.config.student_ou, config_file.config.base_ou
            )
            .as_str(),
            &|search_entry| {
                let compare_name = |name: &String| {
                    name.to_ascii_lowercase()
                        == search_entry.attrs["sAMAccountName"][0].to_ascii_lowercase()
                };
                if !config_file
                    .config
                    .exempt_users
                    .contains(&search_entry.attrs["displayName"][0])
                    && !names.iter().any(&compare_name)
                {
                    warn!(
                        "Unknown user {} ({})",
                        search_entry.attrs["displayName"][0],
                        search_entry.attrs["sAMAccountName"][0]
                    );
                    Ok(true)
                } else {
                    Ok(false)
                }
            },
        )?;

        if num_unknown_users > 0 {
            warn!(
                "Found {} unknown users in {}!",
                num_unknown_users,
                graduation_year(&config_file, grade.0)
            );
        }
        total_num_unknown_users += num_unknown_users;
    }
    if total_num_unknown_users > 0 {
        warn!("Found {} unknown users total!", total_num_unknown_users);
    }
    Ok(())
}

#[derive(Debug)]
struct UnwrapError {}
impl Display for UnwrapError {
    fn fmt(&self, formatter: &mut Formatter) -> FmtResult {
        write!(formatter, "Error performing LDAP")
    }
}
impl Error for UnwrapError {}

fn inner_loop(
    ad: &mut Server,
    config_file: &ConfigFile,
    record: &Record,
    search_result: &SearchEntry,
    changed_passwords_writer: &mut csv::Writer<std::fs::File>,
) -> Result<(), Box<dyn Error>> {
    let distinguished_name = &search_result.attrs["distinguishedName"][0];
    if let Ok(mut object) = ad.get_active_directory_object(distinguished_name) {
        // println!("{}", record.grade);
        let new_grade = &config_file.grades[&record.grade];

        if record.active {
            for grade in &config_file.grades {
                if let Some(groups) = &grade.1.groups {
                    for group in groups {
                        if !new_grade
                            .groups
                            .clone()
                            .and_then(|g| Some(g.contains(&group)))
                            .unwrap_or(false)
                        {
                            if object.remove_from_group(&group).is_err() {
                                info!("Errors editing the groups may not be true errors, but instead an artifact from the users not actually being a part of groups.");
                            }
                        }
                    }
                }
            }

            if new_grade.reset_password.filter(|reset| *reset).is_some() {
                let mut uid = ['0'; 4];
                for (i, c) in format!("{}", record.uid)
                    .chars()
                    .rev()
                    .take(4)
                    .collect::<Vec<_>>()
                    .iter()
                    .rev()
                    .enumerate()
                {
                    uid[i] = c.clone();
                }

                let mut password: String = format!("Bulldogs{}", String::from_iter(&uid));
                password.truncate("Bulldogs".len() + 4);

                let user = object.as_user();

                user.change_password(&password)?;
                user.no_change_password_on_next_login()?;

                changed_passwords_writer.serialize(ChangedPasswordRecord {
                    firstname: record.firstname.clone(),
                    lastname: record.lastname.clone(),
                    grade: record.grade.clone(),
                    school_short_code: record.school_short_code.clone(),
                    password,
                })?;
            }

            if let Some(password) = new_grade.password.clone() {
                let user = object.as_user();

                user.change_password(&password)?;
                user.no_change_password_on_next_login()?;

                changed_passwords_writer.serialize(ChangedPasswordRecord {
                    firstname: record.firstname.clone(),
                    lastname: record.lastname.clone(),
                    grade: record.grade.clone(),
                    school_short_code: record.school_short_code.clone(),
                    password,
                })?;
            }

            if let Some(new_groups) = &new_grade.groups {
                for group in new_groups {
                    if object.add_to_group(&group).is_err() {
                        info!("Errors editing the groups may not be true errors, but instead an artifact from the users not actually being a part of groups.");
                    }
                }
            }

            if new_grade
                .disabled
                .filter(|&should_disable| should_disable)
                .is_some()
            {
                object.as_user().disable_user()?;
            } else {
                object.as_user().enable_user()?;
            }
        } else {
            object.as_user().disable_user()?;
        }

        if let Some(root) = new_grade.root {
            if root {
                object.move_object(&(new_grade.ou.clone()))?;
            } else {
                object.move_object(
                    &(new_grade.ou.clone()
                        + ","
                        + config_file.config.student_ou.as_str()
                        + ","
                        + config_file.config.base_ou.as_str()),
                )?;
            }
        } else {
            object.move_object(
                &(new_grade.ou.clone()
                    + ","
                    + config_file.config.student_ou.as_str()
                    + ","
                    + config_file.config.base_ou.as_str()),
            )?;
        }
    }

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("activedirectoryrefresh")
        .version("0.1")
        .author("Nick Y. <nyahr@otsegops.org>")
        .about("Creates and edits users to match the input csv")
        .arg(Arg::with_name("run")
            .short("r")
            .long("run")
            .help("Actually runs the operations (instead of the test-only mode default)")
        )
        .get_matches();

    *(crate::active_directory::TEST_RUN.write())? = !matches.is_present("run");

    #[cfg(debug_assertions)]
    simple_logger::init_with_level(log::Level::Info)?;
    #[cfg(not(debug_assertions))]
    simple_logging::log_to_file("activedirectoryrefresh.log", log::LevelFilter::Info)?;

    let config = read_toml_file("config.toml");
    if config.is_err() {
        let example_config_toml = include_bytes!("config.toml");
        error!("Could not read file.  Try following example_config.toml file and make sure to name the final result \"config.toml\".");
        fs::File::create("example_config.toml")?.write_all(example_config_toml)?;
    }

    let data = read_csv_file("data.csv");
    if data.is_err() {
        let example_data_csv = include_bytes!("data.csv");
        error!("Could not read file.  Try following example_data.csv file and make sure to name the final result \"data.csv\".");
        fs::File::create("example_data.csv")?.write_all(example_data_csv)?;
    }

    let changed_passwords_file = csv::Writer::from_path("changed_passwords.csv");
    if changed_passwords_file.is_err() {
        error!("Could not open changed_passwords.csv");
    }

    let mut changed_passwords_writer = changed_passwords_file?;
    let mut config_file = config?;
    let config_file_back = config_file.clone();
    for (grade_name, grade) in &mut config_file.grades {
        if let Some(groups) = &mut grade.groups {
            for group in groups.iter_mut() {
                *group = group.replace(
                    "{% year %}",
                    &format!("{}", graduation_year(&config_file_back, &grade_name)).to_string(),
                ) + ","
                    + &config_file.config.security_groups_ou
                    + ","
                    + &config_file.config.base_ou;
            }
        }
    }
    let data_file = data?;

    let mut ad = Server::new(
        &config_file.config.server,
        &config_file.config.account,
        &config_file.config.secret,
    )?;

    for record in data_file.values() {
        info!("Account: {}{}", record.firstname, record.lastname);

        let mut search_result_option = ad.simple_search(
            format!(
                "{},{}",
                config_file.config.student_ou, config_file.config.base_ou
            )
            .as_str(),
            format!("(sAMAccountName={}{})", record.firstname, record.lastname).as_str(),
        )?;

        if search_result_option.is_none() {
            if let Some(current_grade) = &config_file.grades.get(&record.grade) {
                let distinguished_name = format!(
                    "CN={} {},{},{},{}",
                    record.firstname,
                    record.lastname,
                    current_grade.ou,
                    config_file.config.student_ou,
                    config_file.config.base_ou
                );

                let mut uid = ['0'; 4];
                for (i, c) in format!("{}", record.uid)
                    .chars()
                    .rev()
                    .take(4)
                    .collect::<Vec<_>>()
                    .iter()
                    .rev()
                    .enumerate()
                {
                    uid[i] = c.clone();
                }

                let mut password: String = format!("Bulldogs{}", String::from_iter(&uid));
                password.truncate("Bulldogs".len() + 4);

                if ad
                    .add_user(
                        &distinguished_name,
                        &record.firstname,
                        &record.lastname,
                        graduation_year(&config_file, &record.grade),
                    )
                    .is_ok()
                {
                    if let Ok(mut object) = ad.get_active_directory_object(&distinguished_name) {
                        let user = object.as_user();

                        user.change_password(&password)?;
                        user.force_change_password_on_next_login()?;
                        user.enable_user()?;

                        search_result_option = ad.simple_search(
                            format!(
                                "{},{}",
                                config_file.config.student_ou, config_file.config.base_ou
                            )
                                .as_str(),
                            format!("(sAMAccountName={}{})", record.firstname, record.lastname)
                                .as_str(),
                        )?;

                        changed_passwords_writer.serialize(ChangedPasswordRecord {
                            firstname: record.firstname.clone(),
                            lastname: record.lastname.clone(),
                            grade: record.grade.clone(),
                            school_short_code: record.school_short_code.clone(),
                            password,
                        })?;
                    }
                }
            }
        }

        if let Some(search_result) = search_result_option {
            if !config_file
                .config
                .exempt_users
                .contains(&search_result.attrs["name"][0])
            {
                let loop_ret = inner_loop(
                    &mut ad,
                    &config_file,
                    &record,
                    &search_result,
                    &mut changed_passwords_writer,
                );
                if loop_ret.is_err() {
                    error!("Could not complete the user operations");
                }
            }
        }
    }

    get_unknown_users(
        &mut ad,
        &config_file,
        &data_file.keys().cloned().collect::<Vec<_>>(),
    )?;

    Ok(())
}
