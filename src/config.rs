// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    pub account: String,
    pub secret: String,
    pub server: String,
    pub base_ou: String,
    pub student_ou: String,
    pub security_groups_ou: String,
    pub grade_order: Vec<String>,
    pub exempt_users: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Grade {
    pub ou: String,
    pub groups: Option<Vec<String>>,
    pub root: Option<bool>,
    pub disabled: Option<bool>,
    pub password: Option<String>,
    pub reset_password: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ConfigFile {
    pub config: Config,
    pub grades: HashMap<String, Grade>,
}
